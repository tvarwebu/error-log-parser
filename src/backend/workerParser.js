function parseOneError (str, config) {
  if (str.replace(/^\s+|\s+$/g, '').length === 0) {
    return null
  }
  var lines = str.split(/\n/m)
  var parsableEnded = false
  var details = {}
  for (var i = 0; i < lines.length; i++) {
    var line = lines[i]
    var found = false
    for (var j in config['specific']) {
      var re = new RegExp(config['specific'][j]['search'], config['specific'][j]['flags'] || '')
      var key = config['specific'][j]['key']
      if (re.test(line)) {
        if (typeof details[key] === 'undefined') {
          details[key] = ''
        } else {
          details[key] += '\n'
        }
        if (!config['specific'][j].keepSearch) {
          line = line.replace(re, '')
        }
        details[key] += line
        found = true
        break
      }
    }
    if (found) {
      parsableEnded = true
      continue
    }
    if (!parsableEnded) {
      var re2 = new RegExp(config['pairs-separator'], '')
      var re3 = new RegExp(config['pair-separator'], '')
      var pairs = line.split(re2).map((str) => {
        var ind = str.search(re3)
        if (ind !== -1) {
          var k = str.substring(0, ind).replace(/^\s+/, '').replace(/\s*$/, '')
          var v = str.substring(ind + 1).replace(/^\s+/, '').replace(/\s*$/, '')
          if (k.length > 0 && v.length > 0) {
            if (k === config['unparsable'] && (typeof config['unparsable-found-use-rest'] === 'undefined' || config['unparsable-found-use-rest'] === true)) {
              parsableEnded = true
            }
            return {key: k, value: v}
          } else if (ind < config['pair-key-max-length'] || 20) {
            return null
          }
        }
        return str
      }).filter((el) => el)
      var lastKey = null
      for (var l in pairs) {
        if (typeof pairs[l] === 'string') {
          if (lastKey !== null) {
            var splitString = ', '
            var reg = new RegExp(escapeRegExp(details[lastKey]) + '(' + config['pairs-separator'] + ')', '')
            var out = line.match(reg)
            if (out) {
              splitString = out[1]
            }
            details[lastKey] += splitString + pairs[l]
          } else {
            appendDetails(details, config, pairs[l])
          }
        } else {
          lastKey = pairs[l].key
          details[pairs[l].key] = pairs[l].value
        }
      }
    } else {
      appendDetails(details, config, line)
    }
  }
  if (Object.keys(details).length === 0) {
    return null
  }
  if (typeof details[config['unparsable']] !== 'undefined') {
    details[config['unparsable']] = details[config['unparsable']].replace(/^\s+/, '').replace(/\s*$/, '')
  }
  details['__length__'] = str.length
  details['__key__'] = parseOneError.index++
  return details
}
parseOneError.index = 0
function appendDetails (details, config, line) {
  var key = config['unparsable']
  if (typeof details[key] === 'undefined') {
    details[key] = ''
  } else {
    details[key] += '\n'
  }
  details[key] += line
  return details[key]
}
function parseErrors (str, config) {
  if(str[0] === '[' && str[str.length - 1] === ']') {
    try {
      return JSON.parse(str);
    } catch { }
  }
  let reg = new RegExp(config['item-separator'], config['item-separator-flags'])
  return str.split(reg).map((itm) => parseOneError(itm, config)).filter((itm) => itm !== null)
}
addEventListener('message', function (event) {
  var object
  try {
    object = event.data.text
  } catch (e) {
    postMessage({
      error: true
    })
    return
  }
  postMessage({
    onjsonToHTML: true,
    tab: event.data.tab,
    data: parseErrors(object, event.data.config)
  })
}, false)
function escapeRegExp (string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
}
