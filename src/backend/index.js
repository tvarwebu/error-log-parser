function init () {
  chrome.runtime.onConnect.addListener(function (port) {
    port.onMessage.addListener(function (msg) {
      var workerFormatter
      var text = msg.text

      function onWorkerFormatterMessage (event) {
        var message = event.data
        workerFormatter.removeEventListener('message', onWorkerFormatterMessage, false)
        workerFormatter.terminate()
        port.postMessage({
          dataParsed: true,
          data: message.data
        })
      }
      if (msg.init) {
        port.postMessage({
          oninit: true
        })
      }

      if (msg.textToData) {
        workerFormatter = new Worker('../js/workerParser.js', { type: 'module' })
        workerFormatter.addEventListener('message', onWorkerFormatterMessage, false)
        workerFormatter.postMessage({
          text: text,
          fnName: msg.fnName,
          config: msg.config
        })
      }
    })
  })
}

var options = {}
if (localStorage.options) {
  options = JSON.parse(localStorage.options)
}

if (typeof options.addContextMenu === 'undefined') {
  options.addContextMenu = true
  localStorage.options = JSON.stringify(options)
}

init()
