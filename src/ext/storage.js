export default {
  get (key) {
    try {
      return JSON.parse(localStorage.getItem('com.letynsoft.logParser.' + key))
    } catch (e) {}
  },
  set (key, val) {
    try {
      localStorage.setItem('com.letynsoft.logParser.' + key, JSON.stringify(val))
    } catch (e) {}
  },
  remove (key) {
    try {
      localStorage.removeItem('com.letynsoft.logParser.' + key)
    } catch (e) {}
  },
  clear () {
    try {
      localStorage.clear()
    } catch (e) {}
  }
}
