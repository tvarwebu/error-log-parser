import Vue from 'vue'
import root from './root.vue'
import storage from '../ext/storage.js'
import SlideUpDown from 'vue-slide-up-down'
// FIXME: https://developer.chrome.com/extensions/content_scripts (use background to inject to active tab - if matches url or mime)
var splitSize = 20 * 1024 * 1024
var parserConfig = null
var mainOptions = null
var parserConfigs = null

var port = chrome.runtime.connect({name: 'error-log-parser'})
window.updateParserConfigs = function (data, config) {
  var values = {
    'com.letynsoft.logParser.parserConfigs': data,
    'com.letynsoft.logParser.currentParserConfig': config
  }
  chrome.storage.sync.set(values)
}
function displayUI (data) {
  var content = '<style>body, html { padding: 0px; margin: 0px; }</style>'
  content += '<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1" /><div id="root"></div>'
  var parts = window.location.pathname.split('/')
  document.body.innerHTML += content
  document.title = 'Log Viewer: ' + parts[parts.length - 1]
  Vue.component('slide-up-down', SlideUpDown)
  window.vue = new Vue({
    el: '#root',
    render: h => h(root, {
      props: {
        parsedData: data,
        parserConfigs: parserConfigs,
        options: mainOptions,
        currentParserConfigName: parserConfig
      }
    })
  })
}
function* splitStringAsync (string, size, multiline) {
  for (var i = 0; i < string.length; i += size) {
    // TODO: We should wait for the data to be received first
    yield string.substr(i, size)
  }
}
function processData (data) {
  var jsonText

  function formatToHTML (fnName, offset) {
    if (!jsonText) {
      return
    }
    new Promise((resolve, reject) => {
      chrome.storage.sync.get(['com.letynsoft.logParser.parserConfigs', 'com.letynsoft.logParser.currentParserConfig', 'com.letynsoft.logParser.options'], (data) => {
        parserConfig = data['com.letynsoft.logParser.currentParserConfig']
        mainOptions = data['com.letynsoft.logParser.options']
        resolve(data['com.letynsoft.logParser.parserConfigs'] || [])
      })
    }).then(async (configs) => {
      parserConfigs = configs
      if (Object.keys(configs).length === 0) {
        configs.push({
          'name': 'default',
          'item-separator': '^\\s*$',
          'pairs-separator': '[|,]',
          'pair-separator': '[:]',
          'specific': [
            {
              'search': '^POST:',
              'key': 'post'
            },
            {
              'search': '^#([0-9]+)',
              'key': 'backtrace'
            },
            {
              'search': '^message:',
              'key': 'message'
            }
          ],
          'unparsable': 'message'
        })
      }
      var localCfg = storage.get('currentParserConfig')
      if (typeof configs.find(cfg => cfg.name === localCfg) !== 'undefined') {
        parserConfig = localCfg
      } else if (parserConfig === null || typeof configs.find(cfg => cfg.name === parserConfig) === 'undefined') {
        parserConfig = configs.find(item => true).name
      }
      if (jsonText.length > splitSize) {
        // We need to split the content into chunks so it can be passed to the backend process and retrieved back...
        for await (const str of splitStringAsync(jsonText, splitSize, true)) {
          port.postMessage({
            textToData: true,
            text: str,
            fnName: fnName,
            offset: offset,
            config: configs.find(itm => itm.name === parserConfig)
          })
        }
      } else {
        port.postMessage({
          textToData: true,
          text: jsonText,
          fnName: fnName,
          offset: offset,
          config: configs.find(itm => itm.name === parserConfig)
        })
      }
    })
  }

  if (window === top) {
    jsonText = data.text
    formatToHTML(data.fnName, data.offset)
  }
}

function init (data) {
  port.onMessage.addListener(function (msg) {
    if (msg.dataParsed) {
      if (msg.data) {
        if (window.vue) {
          window.vue.$children[0].appendData(msg.data)
        } else {
          displayUI(msg.data)
        }
      } else if (msg.json) {
        port.postMessage({
          getError: true,
          json: msg.json
        })
      }
    }
    if (msg.oninit) {
      processData(data)
    }
    if (msg.ongetError) {
      // displayError(msg.error, msg.loc, msg.offset)
    }
  })
  port.postMessage({
    init: true
  })
}
var loadInterval
function grabDataAndInit () {
  var child, data
  child = document.body.children.length ? document.body.childNodes[0] : document.body
  data = child.innerText
  child.innerText = ''
  if (data) {
    init({text: data, offset: 0})
  }
}
function load (port) {
  // Consider starting not on end of load but on the begining and catch the contents as they are being downloaded?
  var didInit = false
  if (document.body && /errors([^/]+).txt/.exec(document.URL)) {
    grabDataAndInit()
    didInit = true
  }
  if (document.readyState === 'complete' && typeof loadInterval !== 'undefined') {
    clearInterval(loadInterval)
    if (!didInit) {
      chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {
          if (request.parse === true) {
            if (document.body) {
              grabDataAndInit()
            }
          }
        }
      )
    }
  }
}
if (document.readyState === 'complete') {
  load(port)
} else {
  loadInterval = setInterval(() => load(port), 1000)
}
