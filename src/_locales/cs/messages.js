module.exports = {
  'extName': {
    'message': 'Prohlížeč logů',
    'description': 'Jméno rozšíření'
  },
  'extDescription': {
    'message': 'Parsuje jednoduché TXT soubory a zobrazuje je v příjemném GUI',
    'description': 'Extension Description'
  },
  /* content/configs-component.vue */
  'Keys': {
    'message': 'Klíče',
    'description': 'Name of the section with all the key settings'
  },
  'rename': {
    'message': 'přejmenovat',
    'description': 'Configs'
  },
  'export': {
    'message': 'export',
    'description': 'Configs'
  },
  'clone': {
    'message': 'kopírovat',
    'description': 'Configs'
  },
  'delete': {
    'message': 'smazat',
    'description': 'Configs'
  },
  'Available_Keys': {
    'message': 'Dostupné klíče',
    'description': 'Configs'
  },
  'Settings': {
    'message': 'Nastavení',
    'description': 'Configs'
  },
  'Used_in_Group_Header': {
    'message': 'Použitý v hlavičce skupiny',
    'description': 'Configs'
  },
  'Used_in_Item_Header': {
    'message': 'Použitý v hlavičce položky',
    'description': 'Configs'
  },
  'Used_in_Item_Details': {
    'message': 'Použitý v detailu položky',
    'description': 'Configs'
  },
  'Group_keys': {
    'message': 'Klíče skupiny',
    'description': 'Configs'
  },
  'Item_keys': {
    'message': 'Klíče položky',
    'description': 'Configs'
  },
  'Detail_keys': {
    'message': 'Klíče detailu',
    'description': 'Configs'
  },
  'Add_new_compound_Key': {
    'message': 'Vytvořit spojený klíč',
    'description': 'Configs'
  },
  'New_compound_key': {
    'message': 'Nový spojený klíč',
    'description': 'Configs Compound Key'
  },
  'Name': {
    'message': 'Jméno',
    'description': 'Compound Key'
  },
  'Keys': {
    'message': 'Klíče',
    'description': 'Compound Key'
  },
  'select': {
    'message': '-- vyberte --',
    'description': 'Compound Key'
  },
  'select_aditional': {
    'message': '-- vyberte další --',
    'description': 'Compound Key'
  },
  'Regular_Expression': {
    'message': 'Regulární výraz',
    'description': 'Compound Key Replace'
  },
  'Replace_To_eg_1': {
    'message': 'Nahradit za (např. $1)',
    'description': 'Compound Key Replace'
  },
  'Flags': {
    'message': 'Vlajky',
    'description': 'Compound Key Replace'
  },
  'Field_type': {
    'message': 'Typ Pole',
    'description': 'Field Type '
  },
  'Default': {
    'message': 'Výchozí',
    'description': 'Field Type'
  },
  'Date': {
    'message': 'Datum',
    'description': 'Field Type'
  },
  'Request_Identifier': {
    'message': 'Identifikace Dotazu (ID)',
    'description': 'Field Type'
  },
  'Request_Error_Number': {
    'message': 'Identifikace Dotazu (počet)',
    'description': 'Field Type'
  },
  'The_identifier_for_this_field': {
    'message': 'Pole ID pro tento počet',
    'description': 'Field Type'
  },
  'Display_Chart': {
    'message': 'Zobrazit graf',
    'description': 'Field Type'
  },
  'Yes': {
    'message': 'Ano',
    'description': 'Field Type Display Chart'
  },
  'No': {
    'message': 'Ne',
    'description': 'Field Type Display Chart'
  },
  'Date_Format': {
    'message': 'Formát datumu',
    'description': 'Field Type'
  },
  'Auto': {
    'message': 'Automatický',
    'description': 'Field Type - Date Format'
  },
  /* content/root.vue */
  'Toggle_Items_in_list': {
    'message': 'Zobraz/Skryj položky v seznamu',
    'description': 'Bottom actions (Clear Merked Items)'
  },
  'Clear_Resolved_Items': {
    'message': 'Vyčistit vyřešené položky',
    'description': 'Bottom actions (Clear Merked Items)'
  },
  'Items_marked_as_resolved': {
    'message': 'Označit jako vyřešené',
    'description': 'Clear Merked Items'
  },
  'Clear_marked_items': {
    'message': 'Vyčistit označené položky',
    'description': 'Clear Merked Items'
  },
  'Select_the_Related_key': {
    'message': 'Vyberte klíč pro společné položky',
    'description': 'Related Items Popup'
  },
  /* content/filter-component.vue */
  'Any': {
    'message': '-- jakýkoliv --',
    'description': 'Dropdown filter'
  },
  'Type_in_keyword': {
    'message': 'Zadejte klíčové slovo',
    'description': 'Input filter'
  },
  /* content/group-component.vue */
  'Mark_Group': {
    'message': 'Označit',
    'description': 'Group Mark'
  },
  /* content/item-component.vue */
  'Related': {
    'message': 'Spojené',
    'description': 'When related are present'
  },
  /* content/parser-config-component.vue */
  'Parser': {
    'message': 'Parser',
    'description': 'Name of the module'
  },
  'Key_Name': {
    'message': 'Jméno klíče',
    'description': 'Specific Item'
  },
  'Keep_the_Search_expression_in_value': {
    'message': 'Ponechat ve výsledku',
    'description': 'Specific Item'
  },
  'Add_Item': {
    'message': 'Přidat',
    'description': 'Specific Item - Button'
  },
  'Block_Separator': {
    'message': 'Rozdělení položek',
    'description': 'Field Names'
  },
  'Pairs_Separator': {
    'message': 'Rozdělení na páry',
    'description': 'Field Names'
  },
  'Key_Value_Separator': {
    'message': 'Rozdělení klíč/hodnota',
    'description': 'Field Names'
  },
  'Unparsable': {
    'message': 'Neparsovatelné',
    'description': 'Field Names'
  },
  'Specific': {
    'message': 'Specifické',
    'description': 'Field Names'
  },
  /* popup/root.vue */
  'Options': {
    'message': 'Nastavení',
    'description': 'Popup Button'
  },
  /* options/root.vue */
  'Log_Viewer_Settings': {
    'message': 'Nastavení prohlížeče logů',
    'description': 'Options Title'
  },
  'Clear_Script_path': {
    'message': 'Cesta k mazacímu skriptu',
    'description': 'Options Field'
  },
  'Specify_the_path_to_the_clear_script_which_is_able_to_delete_the_specified_items_from_the_log': {
    'message': 'Zadejte cestu mazacího skriptu který umí smazat zadané položky z logů',
    'description': 'Options Field Description'
  },
  'Clear_script_Combined_Keys_Param': {
    'message': 'Parametr pro kombinovaný klíč',
    'description': 'Options Field'
  },
  'Specify_the_argument_to_send_to_the_clear_script_when_multiple_texts_needs_to_be_found_in_one_log_item_when_using_combined_keys': {
    'message': 'Zaddejte jméno argumentu který se má poslat mazacímu skriptu když je nutné hledat podle více klíčů (např. při použití kombinovaného klíče)',
    'description': 'Options Field Description'
  }
}
