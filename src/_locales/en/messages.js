module.exports = {
  'extName': {
    'message': 'Log Viewer',
    'description': 'Extension name'
  },
  'extDescription': {
    'message': 'Allows you to parse simple txt files and display that in a nice GUI',
    'description': 'Extension Description'
  },
  /* content/configs-component.vue */
  'Keys': {
    'message': 'Keys',
    'description': 'Name of the section with all the key settings'
  },
  'rename': {
    'message': 'rename',
    'description': 'Configs'
  },
  'export': {
    'message': 'export',
    'description': 'Configs'
  },
  'clone': {
    'message': 'clone',
    'description': 'Configs'
  },
  'delete': {
    'message': 'delete',
    'description': 'Configs'
  },
  'Available_Keys': {
    'message': 'Available Keys',
    'description': 'Configs'
  },
  'Settings': {
    'message': 'Settings',
    'description': 'Configs'
  },
  'Used_in_Group_Header': {
    'message': 'Used in Group Header',
    'description': 'Configs'
  },
  'Used_in_Item_Header': {
    'message': 'Used in Item Header',
    'description': 'Configs'
  },
  'Used_in_Item_Details': {
    'message': 'Used in Item Details',
    'description': 'Configs'
  },
  'Group_keys': {
    'message': 'Group keys',
    'description': 'Configs'
  },
  'Item_keys': {
    'message': 'Item keys',
    'description': 'Configs'
  },
  'Detail_keys': {
    'message': 'Detail keys',
    'description': 'Configs'
  },
  'Add_new_compound_Key': {
    'message': 'Add new compound Key',
    'description': 'Configs'
  },
  'New_compound_key': {
    'message': 'New compound key',
    'description': 'Configs Compound Key'
  },
  'Name': {
    'message': 'Name',
    'description': 'Compound Key'
  },
  'select': {
    'message': '-- select --',
    'description': 'Compound Key'
  },
  'select_aditional': {
    'message': '-- select aditional --',
    'description': 'Compound Key'
  },
  'Regular_Expression': {
    'message': 'Regular Expression',
    'description': 'Compound Key Replace'
  },
  'Replace_To_eg_1': {
    'message': 'Replace To (eg. $1)',
    'description': 'Compound Key Replace'
  },
  'Flags': {
    'message': 'Flags',
    'description': 'Compound Key Replace'
  },
  'Field_type': {
    'message': 'Field type',
    'description': 'Field Type '
  },
  'Default': {
    'message': 'Default',
    'description': 'Field Type'
  },
  'Date': {
    'message': 'Date',
    'description': 'Field Type'
  },
  'Request_Identifier': {
    'message': 'Request Identifier',
    'description': 'Field Type'
  },
  'Request_Error_Number': {
    'message': 'Request Error Number',
    'description': 'Field Type'
  },
  'The_identifier_for_this_field': {
    'message': 'The identifier for this field',
    'description': 'Field Type'
  },
  'Display_Chart': {
    'message': 'Display Chart',
    'description': 'Field Type'
  },
  'Yes': {
    'message': 'Yes',
    'description': 'Field Type Display Chart'
  },
  'No': {
    'message': 'No',
    'description': 'Field Type Display Chart'
  },
  'Date_Format': {
    'message': 'Date Format',
    'description': 'Field Type'
  },
  'Auto': {
    'message': 'Auto',
    'description': 'Field Type - Date Format'
  },
  /* content/root.vue */
  'Toggle_Items_in_list': {
    'message': 'Toggle Items in list',
    'description': 'Bottom actions (Clear Merked Items)'
  },
  'Clear_Resolved_Items': {
    'message': 'Clear Resolved Items',
    'description': 'Bottom actions (Clear Merked Items)'
  },
  'Items_marked_as_resolved': {
    'message': 'Items marked as resolved',
    'description': 'Clear Merked Items'
  },
  'Clear_marked_items': {
    'message': 'Clear marked items',
    'description': 'Clear Merked Items'
  },
  'Select_the_Related_key': {
    'message': 'Select the Related key',
    'description': 'Related Items Popup'
  },
  /* content/filter-component.vue */
  'Any': {
    'message': '-- any --',
    'description': 'Dropdown filter'
  },
  'Type_in_keyword': {
    'message': 'Type in Keyword',
    'description': 'Input filter'
  },
  /* content/group-component.vue */
  'Mark_Group': {
    'message': 'Mark Group',
    'description': 'Group Mark'
  },
  /* content/item-component.vue */
  'Related': {
    'message': 'Related',
    'description': 'When related are present'
  },
  /* content/parser-config-component.vue */
  'Parser': {
    'message': 'Parser',
    'description': 'Name of the module'
  },
  'Key_Name': {
    'message': 'Key Name',
    'description': 'Specific Item'
  },
  'Keep_the_Search_expression_in_value': {
    'message': 'Keep the Search expression in value',
    'description': 'Specific Item'
  },
  'Add_Item': {
    'message': 'Add Item',
    'description': 'Specific Item - Button'
  },
  'Block_Separator': {
    'message': 'Block Separator',
    'description': 'Field Names'
  },
  'Pairs_Separator': {
    'message': 'Pairs Separator',
    'description': 'Field Names'
  },
  'Key_Value_Separator': {
    'message': 'Key Value Separator',
    'description': 'Field Names'
  },
  'Unparsable': {
    'message': 'Unparsable',
    'description': 'Field Names'
  },
  'Specific': {
    'message': 'Specific',
    'description': 'Field Names'
  },
  /* popup/root.vue */
  'Options': {
    'message': 'Options',
    'description': 'Popup Button'
  },
  /* options/root.vue */
  'Log_Viewer_Settings': {
    'message': 'Log Viewer Settings',
    'description': 'Options Title'
  },
  'Clear_Script_path': {
    'message': 'Clear Script path',
    'description': 'Options Field'
  },
  'Specify_the_path_to_the_clear_script_which_is_able_to_delete_the_specified_items_from_the_log': {
    'message': 'Specify the path to the clear script, which is able to delete the specified items from the log',
    'description': 'Options Field Description'
  },
  'Clear_script_Combined_Keys_Param': {
    'message': 'Clear script Combined Keys Param',
    'description': 'Options Field'
  },
  'Specify_the_argument_to_send_to_the_clear_script_when_multiple_texts_needs_to_be_found_in_one_log_item_when_using_combined_keys': {
    'message': 'Specify the argument to send to the clear script when multiple texts needs to be found in one log item (when using combined keys)',
    'description': 'Options Field Description'
  }

}
